@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
     <div class="col-md-8 col-md-offset-2">
         <div class="panel panel-default">
            <div class="panel-heading clearfix">
                Feature List
                <div class="pull-right text-center">
                     <a href="{{ route('home.index') }}" type="button" class="btn ">Atrás</a>
                </div>
            </div>
            <div class="panel-heading clearfix">
                <div class="form-group">
                  <label for="name"><strong>Name:</strong> {{ $character -> name }}</label>
                </div>
               <div class="form-group">
                 <label for="skin_color">Skin Color: {{ $character -> skin_color }}</label>
               </div>
               <div class="form-group">
                 <label for="eye_color">Eye Color: {{ $character -> eye_color }}</label>
               </div>
               <div class="form-group">
                 <label for="birth_year">Birth Year: {{ $character -> birth_year }}</label>
               </div>
               <div class="form-group">
                 <label for="gender">Gender: {{ $character -> gender }}</label>
               </div>
           </div>
     </div>
 </div>
</div>
@endsection
