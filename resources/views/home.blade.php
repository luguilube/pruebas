@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    Character List
                </div>
                @include('flash::message')
                <div class="panel-body">
                    <label for="amount_limit">StarWars</label>
                    <form action="{{ route('home.filter') }}" method="POST" >
                      {{ csrf_field() }}
                      <div class="row">
                          <div class="col-md-12">
                              <label for="species" class="control-label">Species Filter</label>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-9">
                              <select id="species" name="species" class="form-control">
                                  <option value="All">All</option>
                                  @php
                                      $i = 1;
                                  @endphp
                                  @foreach($species as $data)
                                      <option value="{{ $data }}">{{ $data }}</option>
                                      @php
                                          $i++;
                                      @endphp
                                  @endforeach
                              </select>
                          </div>
                          <div class="col-md-3">
                              <button type="submit" class="btn btn-default btn-block btn-primary">Buscar</button>
                          </div>
                      </div>
                    </form>
                    @include('table')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
