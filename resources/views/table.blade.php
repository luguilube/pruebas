<table class="table table-hover text-center">
    <thead>
        <th class="text-center">Name</th>
        <th class="text-center">Height</th>
        <th class="text-center">Mass</th>
        <th class="text-center">Hair Color</th>
    </thead>
    <tbody>
        @if (isset($characters))
            @php
                $i = 1;
            @endphp
            @foreach ($characters as $key)
                <tr>

                    <td class="text-capitalize"><a href="{{ route('home.show',$i) }}">
                        @if ($key->gender == 'male')
                            <i class="fa fa-male" aria-hidden="true"></i>
                        @elseif ($key->gender == 'female')
                            <i class="fa fa-female" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-android" aria-hidden="true"></i>
                        @endif {{ $key->name }}</a></td>

                    <td>{{ $key->height }}</td>
                    <td>{{ $key->mass }}</td>
                    <td>{{ $key->hair_color }}</td>
                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
        @else
            <tr>
                <label for="sin_data">No hay personajes.</label>
            </tr>
        @endif
    </tbody>
</table>
