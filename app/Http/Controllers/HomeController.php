<?php

namespace SWA\Http\Controllers;

use Illuminate\Http\Request;
use SWA\User;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra la página inicial del sistema
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client(['base_uri' => 'https://swapi.co/api/']);

        $response = $client->request('GET', 'people');
        $characters = json_decode($response->getBody()->getContents())->results;

        $species = HomeController::getSpecies($characters);

        return view('home')->with([
            'characters' => $characters,
            'species' => $species
        ]);
    }

    /**
     * Permite redirijir a la vista que permitira registrar el prestamo
     */
    public function create()
    {
        //
    }

    /**
     * Método que permite registrar el pago.
     * @param [CreateRequest] parametro que permite validar los datos
     * del formulario a través del Forn Request de laravel
     * @var [$request] Request con todos los datos provenientes del formulario
     * de solicitud de credito.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Método que permitira mostrar los datos especificos de un prestamo.
     */
    public function show($id)
    {
        $client = new Client(['base_uri' => 'https://swapi.co/api/']);

        $response = $client->request('GET', 'people/'.$id);
        $character = json_decode($response->getBody()->getContents());

        return view('show')->with([
            'character' => $character
        ]);
    }

    public function filter(Request $request)
    {
        $client = new Client(['base_uri' => 'https://swapi.co/api/']);

        $response = $client->request('GET', 'people');
        $characters = json_decode($response->getBody()->getContents())->results;

        if ( strcmp($request->species,"All") !== 0 ) {
            $species = [];
            $characters = HomeController::getDistinct($characters,$request->species,$species);
        }else {
            $species = HomeController::getSpecies($characters);
        }

        return view('home')->with([
            'characters' => $characters,
            'species' => $species
        ]);
    }

    public function getDistinct($all, $particular, &$species)
    {
        $base_url = "https://swapi.co/api/";

        $client = new Client(['base_uri' => $base_url]);

        $result = [];

        foreach ($all as $key){
            $route = substr($key->species[0],strlen($base_url));
            $response = $client->request('GET', $route);
            $specie = json_decode($response->getBody()->getContents());

            if (!in_array($specie->name,$species)) {
                $species[] = $specie->name;
            }

            if ($specie->name == $particular) {
                $result[] = $key;
            }
        }

        return $result;
    }

    public function getSpecies($all)
    {
        $base_url = "https://swapi.co/api/";

        $client = new Client(['base_uri' => $base_url]);

        $distinct = [];

        foreach ($all as $key){
            $route = substr($key->species[0],strlen($base_url));
            $response = $client->request('GET', $route);
            $specie = json_decode($response->getBody()->getContents());

            if (!in_array($specie->name,$distinct)) {
                $distinct[] = $specie->name;
            }
        }

        return $distinct;
    }
}
